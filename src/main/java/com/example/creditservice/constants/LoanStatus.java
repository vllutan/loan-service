package com.example.creditservice.constants;

public enum LoanStatus {
    IN_PROGRESS("IN_PROGRESS"),
    APPROVED("APPROVED"),
    REFUSED("REFUSED");

    public final String label;

    private LoanStatus(String label) {
        this.label = label;
    }
}
