package com.example.creditservice.constants;

import java.util.Objects;

public enum ErrorStatus {
    LOAN_CONSIDERATION("LOAN_CONSIDERATION"),
    LOAN_ALREADY_APPROVED("LOAN_ALREADY_APPROVED"),
    TRY_LATER("TRY_LATER"),
    TARIFF_NOT_FOUND("TARIFF_NOT_FOUND"),
    ORDER_NOT_FOUND("ORDER_NOT_FOUND"),
    ORDER_IMPOSSIBLE_TO_DELETE("ORDER_IMPOSSIBLE_TO_DELETE");

    public final String label;

    private ErrorStatus(String label) {
        this.label = label;
    }
}
