package com.example.creditservice.controller;

import com.example.creditservice.dto.LoanOrderDto;
import com.example.creditservice.model.LoanCreationForm;
import com.example.creditservice.model.LoanOrder;
import com.example.creditservice.model.LoanResultForm;
import com.example.creditservice.service.LoanService;
import com.example.creditservice.service.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@RequestMapping("/loan-service")
public class LoanWebsiteController {

    private final TariffService tariffService;

    private final LoanService loanService;

    private final LoanController loanController;

    @GetMapping("/index")
    public String index(Model model/*, @ModelAttribute("chosenTariff") TariffDto tariff*/){
        model.addAttribute("tariffs", tariffService.findAllDto());
        return "order";
    }

    @GetMapping("/formOrder")
    public String formOrder(Model model, @ModelAttribute("loanForm") LoanCreationForm loanCreationForm){
        return "loanFormation";
    }

    @PostMapping("/createOrderBody")
    public String createOrderBody(Model model, @ModelAttribute("loanForm") LoanCreationForm loanCreationForm){
        Map<String, String> body = new HashMap<>();
        body.put("userId", loanCreationForm.getUserId());
        body.put("tariffId", loanCreationForm.getTariffId());
        ResponseEntity<?> response = loanController.makeOrder(body);
        return "redirect:/getOrders?userId=" + loanCreationForm.getUserId();
    }

    @GetMapping("/getOrders")
    public String formOrder(Model model, @RequestParam Long userId,
                            @ModelAttribute("loanToDelete") LoanOrderDto loanOrderDto){
        model.addAttribute("orders", loanService.findAllDtoByUserId(userId));
        return "personalPage";
    }

    @GetMapping("/deleteSelectedOrder")
    public String deleteSelectedOrder(Model model, @ModelAttribute("loanToDelete")LoanOrderDto loanOrderDto){
        Map<String, String> body = new HashMap<>();
        body.put("userId", loanOrderDto.getUserId().toString());
        body.put("orderId", loanOrderDto.getOrderId().toString());
        ResponseEntity<?> response = loanController.deleteOrder(body);
        return "redirect:/getOrders?userId=" + loanOrderDto.getUserId();
    }
}
