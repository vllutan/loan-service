package com.example.creditservice.controller;


import com.example.creditservice.dto.TariffDto;
import com.example.creditservice.model.Tariff;
import com.example.creditservice.repository.TariffRepository;
import com.example.creditservice.service.LoanService;
import com.example.creditservice.service.TariffService;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@RequestMapping("/loan-service")
public class LoanController {

    private final TariffService tariffService;
    private final LoanService loanService;

    @GetMapping("/getTariffs")
    public ResponseEntity<?> getTariffs(){
        Map<String, Object> result = tariffService.findAll();
        return ResponseEntity.ok(result);
    }

    @PostMapping("/order")
    public ResponseEntity<?> makeOrder(@RequestBody Map<String, String> body){
        Map<String, Object> orderId = loanService.formOrder(body);
        return ResponseEntity.ok(orderId);
    }

    @GetMapping("/getStatusOrder")
    public ResponseEntity<?> getStatusOrder(@RequestParam UUID orderId){
        Map<String, Object> result = loanService.getStatusOrder(orderId);
        return ResponseEntity.ok(result);
    }

    @DeleteMapping("/deleteOrder")
    public ResponseEntity<?> deleteOrder(@RequestBody Map<String, String> body){
        UUID orderId = loanService.deleteOrder(body);
        return ResponseEntity.ok(orderId);
    }

    @GetMapping("/getAllOrders")
    public ResponseEntity<?> getStatusOrder(){
        Map<String, Object> result = loanService.getAllOrders();
        return ResponseEntity.ok(result);
    }

}
