package com.example.creditservice.controller;

import com.example.creditservice.error.ErrorFormat;
import com.example.creditservice.error.LoanStatusException;
import com.example.creditservice.error.CustomNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Objects;

import static com.example.creditservice.constants.ErrorStatus.*;
import static com.example.creditservice.staticMethod.JsonWrapper.responseWrapper;

@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler({LoanStatusException.class})
    protected ResponseEntity<Object> handleLoanStatusEx(RuntimeException ex, WebRequest request) {
        String message;
        if (Objects.equals(ex.getMessage(), LOAN_CONSIDERATION.label)) {
            message = "Кредит находится на рассмотрении.";
        } else if (Objects.equals(ex.getMessage(), LOAN_ALREADY_APPROVED.label)) {
            message = "Кредит уже одобрен.";
        } else if (Objects.equals(ex.getMessage(), TRY_LATER.label)) {
            message = "В кредите отказано. Попробуйте через 2 минуты.";
        } else {
            message = "Свяжитесь с тех.поддержкой.";
        }
        ErrorFormat tariffError = new ErrorFormat(409, ex.getMessage(), message);
        return new ResponseEntity<>(responseWrapper("error", tariffError), HttpStatus.CONFLICT);
    }

    @ExceptionHandler({CustomNotFoundException.class})
    protected ResponseEntity<Object> handleTariffNotFoundEx(RuntimeException ex, WebRequest request) {
        String message;
        if(Objects.equals(ex.getMessage(), TARIFF_NOT_FOUND.label)){
            message = "Тариф не найден.";
        }
        else if (Objects.equals(ex.getMessage(), ORDER_NOT_FOUND.label)){
            message = "Заявка не найдена.";
        }
        else {
            message = "Свяжитесь с тех.поддержкой.";
        }
        ErrorFormat tariffError = new ErrorFormat(404, ex.getMessage(), message);
        return new ResponseEntity<>(responseWrapper("error", tariffError), HttpStatus.NOT_FOUND);
    }

}
