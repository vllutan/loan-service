package com.example.creditservice.error;

public class CustomNotFoundException extends RuntimeException{
    public CustomNotFoundException(String code){
        super(code);
    }
}
