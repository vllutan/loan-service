package com.example.creditservice.error;

public class LoanStatusException extends RuntimeException{

    public LoanStatusException(String code){
        super(code);
    }
}
