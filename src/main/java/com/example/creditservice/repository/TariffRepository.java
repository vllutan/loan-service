package com.example.creditservice.repository;

import com.example.creditservice.model.Tariff;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface TariffRepository extends JpaRepository<Tariff, Long> {

    ArrayList<Tariff> findAll();
}
