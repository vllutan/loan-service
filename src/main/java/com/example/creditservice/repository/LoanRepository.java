package com.example.creditservice.repository;

import com.example.creditservice.model.LoanOrder;
import com.example.creditservice.model.Tariff;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface LoanRepository extends JpaRepository<LoanOrder, Long> {

    String FIND_LAST_BY_USERID_AND_TARIFFID =
            "select * from loan_order " +
            "where user_id=:userId and tariff_id=:tariffId " +
            "order by id desc limit 1";

    @Query(value = FIND_LAST_BY_USERID_AND_TARIFFID, nativeQuery = true)
    Optional<LoanOrder> findLastByUserIdAndTariffId(Long userId, Long tariffId);

    Optional<LoanOrder> findByOrderId(UUID orderId);

    Optional<LoanOrder> findByUserIdAndOrderId(Long userId, UUID orderId);

    ArrayList<LoanOrder> getAllByStatus(String label);

    ArrayList<LoanOrder> findAll();

    ArrayList<LoanOrder> findAllByUserId(Long userId);
}
