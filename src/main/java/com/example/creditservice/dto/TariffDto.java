package com.example.creditservice.dto;

import jakarta.persistence.*;
import lombok.Data;
import lombok.NonNull;

@Data
public class TariffDto {

    @NonNull
    private Long id;

    @NonNull
    private String type;

    @NonNull
    private String interestRate;


}
