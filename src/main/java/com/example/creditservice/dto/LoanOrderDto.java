package com.example.creditservice.dto;

import com.example.creditservice.model.Tariff;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
public class LoanOrderDto {
    private Long id;

    private String orderId;

    private Long userId;

    private Tariff tariffId;

    private Double creditRating;

    private String status;

    private LocalDateTime timeInsert;

    private LocalDateTime timeUpdate;

    public LoanOrderDto(){
        timeInsert = LocalDateTime.now();
    }

}
