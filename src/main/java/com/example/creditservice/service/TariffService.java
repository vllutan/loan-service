package com.example.creditservice.service;

import com.example.creditservice.dto.TariffDto;

import java.util.ArrayList;
import java.util.Map;

public interface TariffService {
    Map<String, Object> findAll();

    ArrayList<TariffDto> findAllDto();
}
