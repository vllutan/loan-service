package com.example.creditservice.service.impl;

import com.example.creditservice.dto.TariffDto;
import com.example.creditservice.model.Tariff;
import com.example.creditservice.repository.TariffRepository;
import com.example.creditservice.service.TariffMapper;
import com.example.creditservice.service.TariffService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.example.creditservice.staticMethod.JsonWrapper.responseWrapper;

@Service
@RequiredArgsConstructor
public class TariffServiceImpl implements TariffService {

    private final TariffRepository tariffRepository;

    private final TariffMapper tariffMapper;

    @Override
    public Map<String, Object> findAll(){
        ArrayList<Tariff> allTariffs = tariffRepository.findAll();
        Map<String, ArrayList<Tariff>> result = new HashMap<>();
        result.put("tariffs", allTariffs);
        return responseWrapper("data", result);
    }

    @Override
    public ArrayList<TariffDto> findAllDto(){
        ArrayList<Tariff> allTariffs = tariffRepository.findAll();
        return tariffMapper.toDtoList(allTariffs);
    }

}
