package com.example.creditservice.service.impl;

import com.example.creditservice.dto.LoanOrderDto;
import com.example.creditservice.error.LoanStatusException;
import com.example.creditservice.error.CustomNotFoundException;
import com.example.creditservice.model.LoanOrder;
import com.example.creditservice.model.Tariff;
import com.example.creditservice.repository.LoanRepository;
import com.example.creditservice.repository.TariffRepository;
import com.example.creditservice.service.LoanOrderMapper;
import com.example.creditservice.service.LoanService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.*;

import static com.example.creditservice.constants.ErrorStatus.*;
import static com.example.creditservice.constants.LoanStatus.*;
import static com.example.creditservice.staticMethod.JsonWrapper.responseWrapper;

@Service
@RequiredArgsConstructor
public class LoanServiceImpl implements LoanService {

    private final LoanRepository loanRepository;
    private final TariffRepository tariffRepository;

    private final LoanOrderMapper loanOrderMapper;

    @Override
    public Map<String, Object> formOrder(Map<String, String> body){
        Long userId = Long.parseLong(body.get("userId"));
        Long tariffId = Long.parseLong(body.get("tariffId"));

        Tariff tariff = tariffRepository.findById(tariffId).orElseThrow(() ->
                new CustomNotFoundException(TARIFF_NOT_FOUND.label));

        LoanOrder existingLoanOrder = loanRepository.findLastByUserIdAndTariffId(userId, tariffId).orElse(null);

        if(!Objects.isNull(existingLoanOrder)){
            if(Objects.equals(existingLoanOrder.getStatus(), IN_PROGRESS.label)){
                throw new LoanStatusException(LOAN_CONSIDERATION.label);
            }
            else if (Objects.equals(existingLoanOrder.getStatus(), APPROVED.label)){
                throw new LoanStatusException(LOAN_ALREADY_APPROVED.label);
            }
            else if (Objects.equals(existingLoanOrder.getStatus(), REFUSED.label) &&
                    isTimeGapLessThanTwo(existingLoanOrder.getTimeUpdate(), LocalDateTime.now())){
                throw new LoanStatusException(TRY_LATER.label);
            }
        }

        UUID orderId = UUID.randomUUID();
        Double creditRating = Math.round((Math.random() * (0.90 - 0.10) + 0.10) * 100.0) / 100.0;
        String status = IN_PROGRESS.label;
        LocalDateTime timeInsert = LocalDateTime.now();

        LoanOrder loanOrder = new LoanOrder(0L, orderId, userId, tariff, creditRating, status, timeInsert, timeInsert);
        loanRepository.save(loanOrder);
        Map<String, Object> response = new HashMap<>();
        response.put("orderId", orderId);
        return responseWrapper("data", response);
    }

    @Override
    public Map<String, Object> getStatusOrder(UUID orderId) {
        LoanOrder loanOrder = loanRepository.findByOrderId(orderId)
                .orElseThrow(() -> new CustomNotFoundException(ORDER_NOT_FOUND.label));
        Map<String, Object> response = new HashMap<>();
        response.put("orderStatus", loanOrder.getStatus());
        return responseWrapper("data", response);
    }

    @Override
    public UUID deleteOrder(Map<String, String> body) {
        Long userId = Long.parseLong(body.get("userId"));
        UUID orderId = UUID.fromString(body.get("orderId"));

        LoanOrder loanOrder = loanRepository.findByUserIdAndOrderId(userId, orderId).orElseThrow(() ->
                new CustomNotFoundException(ORDER_NOT_FOUND.label));

        if(!Objects.equals(loanOrder.getStatus(), IN_PROGRESS.label)) {
            throw new LoanStatusException(ORDER_IMPOSSIBLE_TO_DELETE.label);
        }
        loanRepository.delete(loanOrder);
        return orderId;
    }

    @Override
    @Scheduled(fixedDelay = 120000)
    @Async
    public void considerOrders() {
        ArrayList<LoanOrder> loanOrders = loanRepository.getAllByStatus(IN_PROGRESS.label);
        for (LoanOrder order: loanOrders) {
            if(Math.random()<0.5){
                order.setStatus(REFUSED.label);
            }
            else {
                order.setStatus(APPROVED.label);
            }
            order.setTimeUpdate(LocalDateTime.now());
            loanRepository.save(order);
        }
    }

    @Override
    public Map<String, Object> getAllOrders() {
        ArrayList<LoanOrder> loanOrders = loanRepository.findAll();
        return responseWrapper("data", responseWrapper("allOrders", loanOrders));
    }

    @Override
    public ArrayList<LoanOrderDto> findAllDtoByUserId(Long userId) {
        ArrayList<LoanOrder> orders = loanRepository.findAllByUserId(userId);
        return loanOrderMapper.toDtoList(orders);
    }

    private Boolean isTimeGapLessThanTwo(LocalDateTime firstTimestamp, LocalDateTime secondTimestamp){
        Duration dur = Duration.between(firstTimestamp, secondTimestamp);
        return dur.toMinutes() < 2;
    }

}
