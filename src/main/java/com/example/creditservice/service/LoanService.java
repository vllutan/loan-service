package com.example.creditservice.service;

import com.example.creditservice.dto.LoanOrderDto;

import java.util.ArrayList;
import java.util.Map;
import java.util.UUID;

public interface LoanService {

    public Map<String, Object> formOrder(Map<String, String> body);

    public Map<String, Object> getStatusOrder(UUID orderId);

    public UUID deleteOrder(Map<String, String> body);

    public void considerOrders();

    public Map<String, Object> getAllOrders();

    ArrayList<LoanOrderDto> findAllDtoByUserId(Long userId);
}
