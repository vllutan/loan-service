package com.example.creditservice.service;

import com.example.creditservice.dto.TariffDto;
import com.example.creditservice.model.Tariff;
import org.mapstruct.Mapper;

import java.util.ArrayList;

@Mapper(componentModel = "spring")
public interface TariffMapper {
    Tariff toModel(TariffDto tariffDto);
    TariffDto toDto(Tariff tariff);
    ArrayList<Tariff> toModelList(ArrayList<TariffDto> tariffDtoList);
    ArrayList<TariffDto> toDtoList(ArrayList<Tariff> tariffList);
}
