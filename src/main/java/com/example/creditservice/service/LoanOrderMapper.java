package com.example.creditservice.service;

import com.example.creditservice.dto.LoanOrderDto;
import com.example.creditservice.model.LoanOrder;
import org.mapstruct.Mapper;

import java.util.ArrayList;

@Mapper(componentModel = "spring")
public interface LoanOrderMapper  {
    LoanOrder toModel(LoanOrderDto loanOrderDto);
    LoanOrderDto toDto(LoanOrder loanOrder);
    ArrayList<LoanOrder> toModelList(ArrayList<LoanOrderDto> loanOrderDtos);
    ArrayList<LoanOrderDto> toDtoList(ArrayList<LoanOrder> loanOrders);
}
