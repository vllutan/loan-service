package com.example.creditservice.staticMethod;

import java.util.HashMap;
import java.util.Map;

public class JsonWrapper {
    public static Map<String, Object> responseWrapper(String type, Object body){
        Map<String, Object> response = new HashMap<>();
        response.put(type, body);
        return response;
    }
}
